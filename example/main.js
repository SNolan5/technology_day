  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBN8RyDPnaPPaEApxowD1Ci8QmaqPY99ow",
    authDomain: "fir-example-6a5b5.firebaseapp.com",
    databaseURL: "https://fir-example-6a5b5.firebaseio.com",
    projectId: "fir-example-6a5b5",
    storageBucket: "fir-example-6a5b5.appspot.com",
    messagingSenderId: "757756968870"
  };
  firebase.initializeApp(config);

var messagesRef = firebase.database().ref('messages');

document.getElementById('LogInForm').addEventListener('submit', submitForm);

function submitForm(e){
  e.preventDefault();

  var name = getInputVal('name');
  var age = getInputVal('age');
  var gender = getInputVal('gender');
  var message = getInputVal('message');

  saveMessage(name,age,gender,message);

  document.getElementById('LogInForm').reset();
}
function getInputVal(id){
  return document.getElementById(id).value;
}

function saveMessage(name,age,gender,message){
  var newMessageRef = messagesRef.push();
  newMessageRef.set({
    name: name,
    age: age,
    gender: gender,
    message: message
  });
}
